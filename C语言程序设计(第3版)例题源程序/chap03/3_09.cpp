/* 【例3-9】求解简单表达式。输入一个形式如"操作数 运算符 操作数"的四则运算表达式，输出运算结果，要求使用switch语句编写。 */

# include <stdio.h>
int main (void) 
{
    double value1, value2;
    char op;

    printf ("Type in an expression: ");    /* 提示输入一个表达式 */
    scanf ("%lf%c%lf", &value1, &op, &value2);
    switch (op){
        case '+':
            printf ("=%.2f\n", value1 + value2);
            break;
        case '-':
            printf ("=%.2f\n", value1 - value2); 
            break;
        case '*':
            printf ("=%.2f\n", value1 * value2); 
            break;
        case '/':
            printf ("=%.2f\n", value1 / value2); 
            break;
        default: 
            printf ("Unknown operator\n"); 
            break;
    }

    return 0;
}
